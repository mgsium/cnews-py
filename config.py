import os

base_dir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    # API base URL
    API_BASE_ENDPOINT = "http://localhost:5000/api/v1.0"

    # Frontend Repo URL
    GITLAB_FE_REPO_LINK = "https://gitlab.com/mgsium/cnews-py"

    # Backend Repo URL
    GITLAB_BE_REPO_LINK = "https://gitlab.com/mgsium/cnews-api"

    # (Backend) License Link
    GITLAB_LICENSE_LINK = "https://gitlab.com/mgsium/cnews-api/-/blob/master/LICENSE"
