# Third Party Libs
import webbrowser

from pyqtgraph import PlotWidget, mkPen
from PyQt5.QtCore import Qt, QSize, pyqtSlot
from PyQt5.QtGui import QCursor, QPixmap, QIcon
from PyQt5.QtWidgets import QWidget, QMainWindow, QToolBar, QMessageBox, QStackedWidget, QStatusBar, QAction
from PyQt5.QtWidgets import QPushButton, QLabel, QFrame, QSplashScreen
from PyQt5.QtWidgets import QGridLayout, QVBoxLayout, QHBoxLayout
import qtawesome as qta

# Standard Libs
import requests
import sys
from time import sleep

# Local Libs
from components.Auth import LoginDialog
from components.Pages.ArchivePage import ArchivePage
from components.Pages.ArticleSelectionPage import ArticleSelectionPage
from components.Pages.HomePage import HomePage
from components.Pages.NewAggregationPage import NewAggregationPage
from components.Pages.ProfileDialog import ProfileDialog
from components.Pages.ProfilePage import ProfilePage
from config import Config


class MainWindow(QMainWindow):
    """Main Application Window."""

    __slots__ = ("_username", "_password", "_archivePage")

    def __init__(self):
        super().__init__()
        self.setWindowTitle("Central News")
        self.setWindowIcon(QIcon("assets\img\png\cn_icon.png"))
        self.setMinimumSize(1024, 768)

        self._loginProcess()

    def _createMenu(self):
        menu = self.menuBar()

        # Create File Menu
        menuFile = menu.addMenu("File")

        # File Menu: Account Settings Action
        accountSettingsAction = menuFile.addAction("Account Settings")
        accountSettingsAction.triggered.connect(self._openProfileDialog)
        menuFile.addSeparator()
        # File Menu: Logout Action
        logoutAction = menuFile.addAction("Logout")
        logoutAction.triggered.connect(self._logout)
        # File Menu: Exit Action -> Closes the Program
        exitAction = menuFile.addAction("Exit")
        exitAction.triggered.connect(self._invokeExitWindowDialog)

        # Create Advanced Menu
        advancedFile = menu.addMenu("Advanced")

        # Create Help Menu
        helpFile = menu.addMenu("Help")
        helpFile.addAction("License")
        helpFile.triggered.connect(lambda: webbrowser.open(Config.GITLAB_LICENSE_LINK))
        # Help Menu: Gitlab Repo Link
        gitlabRepoLink = helpFile.addAction("Gitlab Repository")
        gitlabRepoLink.triggered.connect(lambda: webbrowser.open(Config.GITLAB_FE_REPO_LINK))

    def _createToolbar(self):
        sideBar = QToolBar()
        sideBar.setStyleSheet(
            """
                QToolBar { font-size: 3mm; background: black; }
                QPushButton { background: black; color: white; } 
                QPushButton:hover { background: rgba(255, 255, 255, 0.3); border-radius: 5px; }
                QIcon { padding: 5px; }
            """
        )

        # Adding Sidebar Actions
        sideBar = self._createUserIcon(sideBar)
        sideBar.addSeparator()
        sideBar = self._createHomeIcon(sideBar)
        sideBar = self._createCreateIcon(sideBar)
        sideBar = self._createArchiveIcon(sideBar)
        sideBar.addSeparator()
        sideBar = self._createExitIcon(sideBar)

        sideBar.setIconSize(QSize(36, 36))  # Changing the Icon Size
        sideBar.setMovable(False)           # Prevent Sidebar Repositioning

        self.addToolBar(Qt.LeftToolBarArea , sideBar)

    def _createDisplayWindow(self, splitter):
        displayWindowWidget = QWidget()
        displayWindowLayout = QGridLayout()

        displayWindowLayout.addWidget(QLabel("Display Window"))

        displayWindowWidget.setLayout(displayWindowLayout)
        splitter.addWidget(displayWindowWidget)
        return splitter

    def _createUserPage(self, stack):
        stack.addWidget(self._userPage._createPage())
        return stack

    def _createHomePage(self, stack):
        homePage = QWidget()
        homePage.setLayout(self._homePage._createPage())
        stack.addWidget(homePage)
        return stack

    def _createCreationPage(self, stack):
        splitter = self._newAggregationPage._createPage()
        # Add Widget to Stack
        stack.addWidget(splitter)

        return stack

    def _createSelectionPage(self, stack):
        splitter = self._articleSelectionPage._createPage()
        # Add Widget to Stack
        stack.addWidget(splitter)

        return stack

    def _createArchive(self, stack):
        archive = self._archivePage._createArchive()
        stack.addWidget(archive)

        return stack

    def _createUserIcon(self, sideBar):
        userIcon = qta.icon("fa5s.user", color_active="lightblue", color="whitesmoke")
        sideBar.addWidget(self._createTaskbarNavBtn(userIcon, "User", 1))
        return sideBar

    def _createHomeIcon(self, sideBar):
        homeIcon = qta.icon("fa5s.home", color_active="lightblue", color="whitesmoke")
        sideBar.addWidget(self._createTaskbarNavBtn(homeIcon, "Home", 0))
        return sideBar

    def _createCreateIcon(self, sideBar):
        createIcon = qta.icon("fa5s.camera-retro", color_active="lightblue", color="whitesmoke")
        sideBar.addWidget(self._createTaskbarNavBtn(createIcon, "Create", 2))
        return sideBar

    def _createArchiveIcon(self, sideBar):
        archiveIcon = qta.icon("fa5s.database", color_active="lightblue", color="whitesmoke")
        sideBar.addWidget(self._createTaskbarNavBtn(archiveIcon, "Archive", 3))
        return sideBar

    def _createExitIcon(self, sideBar):
        exitIcon = qta.icon("fa5s.sign-out-alt", color_active="lightblue", color="whitesmoke")
        exitBtn = self._createTaskbarBtn(exitIcon, "Exit")
        exitBtn.clicked.connect(self._invokeExitWindowDialog)                  # Close Window on click
        sideBar.addWidget(exitBtn)
        return sideBar

    def _switchToPage(self, index):
        if index == 3 : self._archivePage.refreshPage() # Refresh Archive Page on Navigation
        self.widgetStack.setCurrentIndex(index)

    def _createControlWindow(self, splitter):
        controlWindowWidget = QWidget()
        controlWindowLayout = QGridLayout()

        controlWindowLayout.addWidget(QLabel("Control Window"))

        controlWindowWidget.setLayout(controlWindowLayout)
        splitter.addWidget(controlWindowWidget)
        return splitter

    def _createStatusBar(self):
        status = QStatusBar()
        self.setStatusBar(status)

    def _createTaskbarBtn(self, icon, tooltipMsg, x=32, y=32):
        taskbarBtn = QPushButton(icon, "")
        taskbarBtn.setIconSize(QSize(x, y))
        taskbarBtn.setToolTip(tooltipMsg)
        taskbarBtn.setCursor(QCursor(Qt.PointingHandCursor))
        return taskbarBtn

    def _createTaskbarNavBtn(self, icon, tooltipMsg, tabIndex, x=32, y=32):
        taskbarBtn = self._createTaskbarBtn(icon, tooltipMsg)
        taskbarBtn.clicked.connect(lambda : self._switchToPage(tabIndex))
        return taskbarBtn

    def _createWidgetStack(self):
        widgetStack = QStackedWidget()

        # Accessible through the Navbar
        widgetStack = self._createHomePage(widgetStack)
        widgetStack = self._createUserPage(widgetStack)
        widgetStack = self._createCreationPage(widgetStack)
        widgetStack = self._createArchive(widgetStack)

        # Intermediate Pages
        widgetStack = self._createSelectionPage(widgetStack)

        # widgetStack.setCurrentIndex(1)
        return widgetStack

    def _createSplashScreen(self):
        splashScreenImg = QPixmap("assets\img\png\splash_screen.png")
        splashScreen = QSplashScreen(splashScreenImg, Qt.WindowStaysOnTopHint)
        splashScreen.setMask(splashScreen.mask())
        return splashScreen

    def _initAuth(self, loginDialog):
        try:
            self._username = loginDialog.username
            self._password = loginDialog.password
            self._authToken = loginDialog.authToken
            self._userId = loginDialog.userId
        except AttributeError as e:
            # Exit the Program
            sys.exit(0)

    def _invokeExitWindowDialog(self):
        choice = QMessageBox.question(self, "Exit", "Are you sure you want to exit?", QMessageBox.Yes | QMessageBox.No)
        if choice == QMessageBox.Yes : self.close()

    def _logout(self):
        self.close()
        mainWindow = MainWindow()

    def _loginProcess(self):
        loginDialog = LoginDialog()  # Initialize Login Dialog
        loginDialog.exec()  # Launch Login Dialog

        splashScreen = self._createSplashScreen()  # Initialize Splash Screen
        splashScreen.show()  # Display Splash Screen

        self._initAuth(loginDialog)  # Initialize Authentication Data

        # Initialize Pages
        self._articleSelectionPage = ArticleSelectionPage(self._userId, self._authToken, self._switchToPage)
        self._newAggregationPage = NewAggregationPage(self._userId, self._authToken, \
                                                      self._articleSelectionPage._setActiveAggregation,
                                                      self._switchToPage)
        self._archivePage = ArchivePage(self._userId, self._authToken, \
                                        self._articleSelectionPage._setActiveAggregation, self._switchToPage)
        self._homePage = HomePage(self._userId, self._authToken, self._switchToPage)
        self._userPage = ProfilePage(self._userId, self._authToken, self._username, self._openProfileDialogTrigger)
        self._createMenu()
        self._createToolbar()

        self.widgetStack = self._createWidgetStack()  # Create navigation interface
        self.setCentralWidget(self.widgetStack)  # Set navigation interface as central widget

        # sleep(5)
        splashScreen.close()  # Close Splash Screen

        self.show()

    def _openProfileDialog(self):
        ## TODO: Add Bio Attribute to User Table
        profileDialog = ProfileDialog(self._userId, self._authToken)
        profileDialog.exec()

    @pyqtSlot()
    def _openProfileDialogTrigger(self, event):
        self._openProfileDialog()
