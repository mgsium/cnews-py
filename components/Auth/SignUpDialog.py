# Third Party Libs
from PyQt5.QtCore import Qt, pyqtSlot
from PyQt5.QtGui import QCursor
from PyQt5.QtWidgets import QDialog, QErrorMessage, QMessageBox
from PyQt5.QtWidgets import QLineEdit
from PyQt5.QtWidgets import QPushButton, QLabel
from PyQt5.QtWidgets import QVBoxLayout, QFormLayout, QGroupBox
from PyQt5.QtWidgets import QGraphicsDropShadowEffect

# Standard Libs
import requests
import re

# Local Libs
from .AuthDialog import AuthDialog
from .AuthExceptions import PasswordsDoNotMatchException, BadRequest, InvalidPasswordException
from config import Config

class SignUpDialog(QDialog, AuthDialog):
    "Sign Up Dialog Window"

    def __init__(self):
        super().__init__()
        self.setWindowTitle("News Central Sign Up")
        self.setFixedSize(360, 600)
        self.setStyleSheet("QDialog { background: white }")
        self._invalidPasswordError = QMessageBox()
        self._invalidPasswordError.setIcon(QMessageBox.Critical)
        self._invalidPasswordError.setText("Error")

        layout = QVBoxLayout()
        layout = self._setDialogHeader(layout, "Sign Up")
        layout = self._setFormWidgets(layout)
        layout = self._setFormMisc(layout)
        self.setLayout(layout)

        self.show()

    def _setFormWidgets(self, layout):
        groupBox = QGroupBox()
        groupBox.setStyleSheet("QGroupBox { background-color: whitesmoke; border-radius: 3px; border: 1px solid gainsboro; }")
        groupBox.setFixedHeight(400)
        groupBox.setGraphicsEffect(self._createGroupBoxShadow())
        formLayout = QVBoxLayout()

        formLayout = self._createUsernameField(formLayout)              # Username Field
        formLayout = self._createPasswordField(formLayout)              # Password Field
        formLayout = self._createPasswordPolicyField(formLayout)        # Password Policy
        formLayout = self._createConfirmPasswordField(formLayout)       # Confirm Password Widgets
        formLayout = self._createpasswordMatchErrorLabel(formLayout)    # Passwords do not Match Error Message
        formLayout = self._createSubmitBtn(formLayout)                  # Submit Button

        groupBox.setLayout(formLayout)
        layout.addWidget(groupBox)

        return layout

    def _setFormMisc(self, layout):
        # Login Message
        miscLayout = QVBoxLayout()
        loginMsg = QLabel("<small><em>Already have an account?</em?</small>")
        loginMsg.setAlignment(Qt.AlignCenter)
        miscLayout.addWidget(loginMsg)

        # Login Button -> Returns to Login Dialog
        loginBtn = QPushButton("Login")
        loginBtn.setCursor(QCursor(Qt.PointingHandCursor))
        loginBtn.setFixedHeight(35)
        loginBtn.clicked.connect(self._closeSignUpDialog)
        miscLayout.addWidget(loginBtn)

        layout.addLayout(miscLayout)
        return layout
    
    def _submitSignUpForm(self):
        try:
            username, password, passwordConfirmation = self._collectFieldData()
            if password != passwordConfirmation: raise PasswordsDoNotMatchException()   # Raise exception if passwords do not match
            if not self._passwordIsValid(password): raise InvalidPasswordException()
            res = self._signupRequest(username,password)                                # Sign Up via API
            if res.status_code != 200: raise BadRequest()                               # Check HTTP Response Code
            self.close()
            ## Show Account Created Message
        except PasswordsDoNotMatchException as e:
            self.passwordMatchErrorLabel.show()         # Show Password Matching Error
            return                                      # Return to dialog
        except InvalidPasswordException as e:
            self._invalidPasswordError.setInformativeText("This Password is Invalid. Please Try Again.")
            self._invalidPasswordError.setWindowTitle("Error")
            self._invalidPasswordError.setWindowFlags(Qt.WindowStaysOnTopHint)
            self._invalidPasswordError.exec_()
            self._clearFields()
            return
        except BadRequest as e:
            self._clearFields()                         # Set fields to empty
    
    def _collectFieldData(self):
        # Get Username, Password & Password Confirmation
        username, password = super()._collectFieldData()
        passwordConfirmation = self.confirmPasswordLineEdit.text()
        return (username, password, passwordConfirmation)

    def _clearFields(self):
        super()._clearFields()
        self.confirmPasswordLineEdit.setText("")

    def _signupRequest(self, username, password):
        return requests.post(f"{Config.API_BASE_ENDPOINT}/addUser", json = {
            "username": username,
            "password": password
        })
    
    def _createUsernameField(self, layout):
        layout.addWidget(QLabel("<h5>Username</h5>"))
        self.usernameLineEdit = super()._createUsernameField()
        self.usernameLineEdit.textChanged.connect(self._hidepasswordMatchErrorLabel)
        layout.addWidget(self.usernameLineEdit)
        return layout

    def _createPasswordField(self, layout):
        layout.addWidget(QLabel("<h5>Password</h5>"))
        self.passwordLineEdit = super()._createPasswordField()
        self.passwordLineEdit.textChanged.connect(self._hidepasswordMatchErrorLabel)
        layout.addWidget(self.passwordLineEdit)
        return layout
    
    def _createPasswordPolicyField(self, layout):
        passwordPolicyLabel = QLabel(
            """
            <small>
                <em>
                    Must be more than 8 characters, and include at least 1 Uppercase and lowercase character.
                </em>
            </small>
            """
        )
        passwordPolicyLabel.setWordWrap(True)
        layout.addWidget(passwordPolicyLabel)
        return layout

    def _createConfirmPasswordField(self, layout):
        layout.addWidget(QLabel("<h5>Confirm Password</h5>"))
        self.confirmPasswordLineEdit = QLineEdit()
        self.confirmPasswordLineEdit.setEchoMode(QLineEdit.Password)
        self.confirmPasswordLineEditFont = self.confirmPasswordLineEdit.font()
        self.confirmPasswordLineEditFont.setPointSize(18)
        self.confirmPasswordLineEdit.setFont(self.confirmPasswordLineEditFont)
        self.confirmPasswordLineEdit.textChanged.connect(self._hidepasswordMatchErrorLabel)
        layout.addWidget(self.confirmPasswordLineEdit)
        return layout
    
    def _createpasswordMatchErrorLabel(self, layout):
        self.passwordMatchErrorLabel = QLabel("<small><em>The Passwords do not match.</em></small>")
        self.passwordMatchErrorLabel.setStyleSheet("color: red")
        self.passwordMatchErrorLabel.hide()
        layout.addWidget(self.passwordMatchErrorLabel)
        return layout

    def _createSubmitBtn(self, layout):
        submitBtn = super()._createSubmitBtn()
        submitBtn.clicked.connect(self._submitSignUpForm)
        layout.addWidget(submitBtn)
        return layout

    @pyqtSlot()
    def _hidepasswordMatchErrorLabel(self):
        self.passwordMatchErrorLabel.hide()

    @pyqtSlot()
    def _closeSignUpDialog(self):
        self.close()

    def _passwordIsValid(self, password: str):
        match = re.search(r'(?=.*[a-z])(?=.*[A-Z])', password)
        return bool(match) and (len(password) >= 8)
