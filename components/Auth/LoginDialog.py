# Third Party Libs
from PyQt5.QtCore import Qt, pyqtSlot
from PyQt5.QtGui import QColor, QFontDatabase, QFont, QCursor
from PyQt5.QtWidgets import QWidget, QMainWindow, QToolBar, QDialog
from PyQt5.QtWidgets import QMenu, QAction, QLineEdit
from PyQt5.QtWidgets import QPushButton, QLabel
from PyQt5.QtWidgets import QGridLayout, QVBoxLayout, QFormLayout, QGroupBox, QHBoxLayout
from PyQt5.QtWidgets import QGraphicsDropShadowEffect
import qtawesome as qta

# Standard Libs
import requests

# Local Libs
from .AuthDialog import AuthDialog
from .SignUpDialog import SignUpDialog
from config import Config

class LoginDialog(QDialog, AuthDialog):
    """Login Dialog Window"""

    __slots__ = ("_username", "_password", "_auth_token")

    def __init__(self):
        super().__init__()
        
        self.setWindowTitle("Central News Login")
        self.setWindowIcon(qta.icon("fa5s.sign-in-alt", color="black"))
        self.setStyleSheet("QDialog { background: white }")
        self.setFixedSize(360, 505)

        layout = QVBoxLayout()
        layout = self._setDialogHeader(layout, "Central News.", "Sign In")
        layout = self._setFormWidgets(layout)
        layout = self._setFormMisc(layout)

        self.setLayout(layout)
        self.show()

    def _setFormWidgets(self, layout):
        groupBox = self._createGroupBox()
        formLayout = QVBoxLayout()

        formLayout = self._createUsernameField(formLayout)  # Username Widgets
        formLayout = self._createPasswordField(formLayout)  # Password Widgets
        formLayout = self._createAuthErrorLabel(formLayout) # Authentication Error Message
        formLayout = self._createSubmitBtn(formLayout)      # Submit Button

        groupBox.setLayout(formLayout)
        layout.addWidget(groupBox)
        return layout

    def _setFormMisc(self, layout):
        # Sign Up Message
        miscLayout = QVBoxLayout()
        signupMsg = QLabel("<small><em>Don't have an account?</em></small>")
        signupMsg.setAlignment(Qt.AlignCenter)
        signupMsg.setFixedHeight(40)
        miscLayout.addWidget(signupMsg)

        # Sign Up Button -> Launches Sign Up Dialog
        signupBtn = QPushButton("Sign Up")
        signupBtn.setCursor(QCursor(Qt.PointingHandCursor))
        signupBtn.setFixedHeight(35)
        signupBtn.clicked.connect(self._openSignUpDialog)
        miscLayout.addWidget(signupBtn)

        layout.addLayout(miscLayout)
        return layout
    
    def _submitLoginForm(self):
        username, password = self._collectFieldData()                           # Get Username & Password
        res = self._loginRequest(username, password)                            # Login via API
        deserialized_res = res.json()                                           # Deserialize from JSON

        # Check HTTP Response Code
        if res.status_code == 200:
            self.username = username
            self.password = password
            self.authToken = deserialized_res["auth_token"]
            self.userId = deserialized_res["user_id"]
            self.close()

        self._clearFields()

        # Show Error Message
        self.authErrorLabel.show()
    
    def _loginRequest(self, username, password):
        res = requests.post(f"{Config.API_BASE_ENDPOINT}/Login", json={
            "username": username,
            "password": password
        })
        return res

    def _createGroupBox(self):
        groupBox = QGroupBox()
        groupBox.setObjectName("loginGroupBox")
        groupBox.setStyleSheet("QGroupBox#loginGroupBox { background-color: whitesmoke; border-radius: 3px; border: 1px solid gainsboro; }")
        groupBox.setFixedHeight(275)
        groupBox.setGraphicsEffect(self._createGroupBoxShadow())
        return groupBox
    
    def _createUsernameField(self, layout):
        layout.addWidget(QLabel("<h5>Username</h5>"))
        self.usernameLineEdit = super()._createUsernameField()
        """
        # TODO: Remove ths later
        self.usernameLineEdit.setText("Test")
        """
        layout.addWidget(self.usernameLineEdit)
        return layout

    def _createPasswordField(self, layout):
        layout.addWidget(QLabel("<h5>Password</h5>"))
        self.passwordWidgetsLayout = QHBoxLayout()
        self.passwordLineEdit = super()._createPasswordField()
        """
        # TODO: Remove ths later
        self.passwordLineEdit.setText("Password")
        """
        self.passwordWidgetsLayout.addWidget(self.passwordLineEdit)

        viewPasswordBtn = QPushButton(qta.icon("fa5s.eye", color="white"), "")
        viewPasswordBtn.setFixedSize(49, 49)
        viewPasswordBtn.setStyleSheet("QPushButton { background: black; }")
        viewPasswordBtn.setCursor(QCursor(Qt.PointingHandCursor))
        viewPasswordBtn.clicked.connect(lambda: self._toggleFieldVisibility(self.passwordLineEdit))
        self.passwordWidgetsLayout.addWidget(viewPasswordBtn)

        passwordWidgets = QWidget()
        passwordWidgets.setLayout(self.passwordWidgetsLayout)
        passwordWidgets.setContentsMargins(0, 0, 0, 0)
        self.passwordWidgetsLayout.setContentsMargins(0, 0, 0, 0)
        self.passwordWidgetsLayout.setSpacing(0)
        layout.addWidget(passwordWidgets)

        return layout
    
    def _createAuthErrorLabel(self, layout):
        self.authErrorLabel = QLabel("<small><em>Your Credentials are Invalid</em></small>")
        self.authErrorLabel.setStyleSheet("color: red")
        self.authErrorLabel.hide()
        layout.addWidget(self.authErrorLabel)
        return layout
    
    def _createSubmitBtn(self, layout):
        submitBtn = super()._createSubmitBtn()
        submitBtn.clicked.connect(self._submitLoginForm)
        layout.addWidget(submitBtn)
        return layout
    
    @pyqtSlot()
    def _hideAuthErrorLabel(self):
        self.authErrorLabel.hide()

    @pyqtSlot()
    def _openSignUpDialog(self):
        s = SignUpDialog()
        s.setWindowFlags(Qt.WindowStaysOnTopHint)
        s.exec()
        self.show()
    
    @property
    def username(self):
        return self._username
    
    @username.setter
    def username(self, username):
        self._username = username
    
    @property
    def password(self):
        return self._password
    
    @password.setter
    def password(self, password):
        self._password = password
    
    @property
    def auth_token(self):
        return self._auth_token
    
    @auth_token.setter
    def auth_token(self, auth_token):
        self._auth_token = auth_token
