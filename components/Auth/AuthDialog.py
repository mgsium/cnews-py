# Third Party Libs
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont, QCursor, QColor
from PyQt5.QtWidgets import QLabel, QLineEdit, QPushButton, QGraphicsDropShadowEffect

from components.Common import CommonEffects


class AuthDialog:

    def _setDialogHeader(self, layout, header, subheader=None, icon=None):
        header = QLabel("<h1 style={'font-weight': 300}>" + header + "</h1>")
        header.setFixedHeight(75)
        header.setAlignment(Qt.AlignCenter)
        layout.addWidget(header)

        if subheader is not None:
            subheader = QLabel("<h4>" + subheader + "</h4>")
            subheader.setFont(QFont("Sanchez"))
            subheader.setAlignment(Qt.AlignCenter)
            layout.addWidget(subheader)

        return layout
    
    def _createUsernameField(self):
        usernameLineEdit = QLineEdit()
        usernameLineEditFont = usernameLineEdit.font()
        usernameLineEditFont.setPointSize(18)
        usernameLineEdit.setFont(usernameLineEditFont)
        return usernameLineEdit
    
    def _createPasswordField(self):
        passwordLineEdit = QLineEdit()
        passwordLineEdit.setEchoMode(QLineEdit.Password)
        passwordLineEditFont = passwordLineEdit.font()
        passwordLineEditFont.setPointSize(18)
        passwordLineEdit.setFont(passwordLineEditFont)
        return passwordLineEdit
    
    def _createSubmitBtn(self):
        submitBtn = QPushButton("Submit")
        submitBtn.setCursor(QCursor(Qt.PointingHandCursor))
        submitBtn.setStyleSheet("QPushButton { background: black; color: white }")
        submitBtn.setFixedHeight(40)
        return submitBtn
    
    def _collectFieldData(self):
        username = self.usernameLineEdit.text()
        password = self.passwordLineEdit.text()
        return (username, password)
    
    def _clearFields(self):
        [field.setText("") for field in [self.usernameLineEdit, self.passwordLineEdit]]
    
    def _toggleFieldVisibility(self, field):
        field.setEchoMode(QLineEdit.Password if field.echoMode() == QLineEdit.Normal else QLineEdit.Normal)
        return field

    def _createGroupBoxShadow(self) -> QGraphicsDropShadowEffect:
        return CommonEffects.createGroupBoxShadow()
