class PasswordsDoNotMatchException(Exception):
    def __init__(self):
        super().__init__("Passwords do not match.")

class InvalidPasswordException(Exception):
    def __init__(self):
        super().__init__("Password is not Valid")

class BadRequest(Exception):
    def __init__(self):
        super().__init__("Request was unsuccessful.")
