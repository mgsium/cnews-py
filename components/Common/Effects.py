from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QGraphicsDropShadowEffect


class CommonEffects:
    """A class to create common styling effects."""

    def __init__(self):
        super()

    @staticmethod
    def createGroupBoxShadow() -> QGraphicsDropShadowEffect:
        groupBoxShadow = QGraphicsDropShadowEffect()
        groupBoxShadow.setOffset(0, 4)
        groupBoxShadow.setBlurRadius(2)
        groupBoxShadow.setColor(QColor(180, 180, 180))
        return groupBoxShadow
