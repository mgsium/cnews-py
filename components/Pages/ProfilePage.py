from datetime import datetime
from dateutil import parser
import requests
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QCursor
from PyQt5.QtWidgets import QWidget, QGridLayout, QFrame, QVBoxLayout, QHBoxLayout, QLabel
import qtawesome as qta
from pyqtgraph import PlotWidget, mkPen

from config import Config


class ProfilePage:
    """A page to display profile information."""

    def __init__(self, user_id, auth_token, username, open_profile_dialog_trigger):
        super()
        self._user_id = user_id
        self._auth_token = auth_token
        self._username = username
        self._openProfileDialogTrigger = open_profile_dialog_trigger

        self._retrieveProfileData()

    def _createPage(self) -> QWidget:
        userPage = QWidget()                    # Initialize Page Widget
        userPageLayout = QGridLayout()          # Initialize Page Layout

        # Create Profile Card
        userPageLayout.addWidget(self._createProfileCard(), 0, 0)
        # Create Summary CArd
        userPageLayout.addWidget(self._createSummaryCard(), 0, 1)
        # Create Stats Plot
        userPageLayout.addWidget(self._createStatsPlot(), 1, 0, 2, 0)

        userPage.setLayout(userPageLayout)      # Set User Page Layout

        return userPage

    def _createProfileCard(self) -> QWidget:
        profileCard = QWidget()                 # Initialize Profile Card Widget
        profileCardLayout = QVBoxLayout()       # Initialize Profile Card Layout

        # Apply Styling
        profileCard.setObjectName("profileCard")
        profileCard.setStyleSheet(
            """
                QWidget#profileCard { background: white; border: 1px solid gainsboro; border-radius: 2px; }
                QWidget#profileCard:hover { border: 1px solid grey; }
            """
        )

        profileCardLayout.addWidget(self._createIdBar(), 6)                         # Profile Id
        profileCardLayout.addWidget(self._createSeparator(), 1)                     # Separator
        profileCardLayout.addWidget(self._createBio(), 14, alignment=Qt.AlignTop)   # Bio

        profileCard.setCursor(QCursor(Qt.PointingHandCursor))
        profileCard.setLayout(profileCardLayout)

        return profileCard

    def _createSummaryCard(self) -> QWidget:
        summaryCard = QWidget()                 # Initialize Summary Card Widget
        summaryCardLayout = QVBoxLayout()       # Initialize Summary Card Layout

        # Apply Styling
        summaryCard.setObjectName("summaryCard")
        summaryCard.setStyleSheet(
            """
                QWidget#summaryCard { background: white; border: 1px solid gainsboro; border-radius: 2px; }
                QWidget#summaryCard:hover { border: 1px solid grey; }
            """
        )

        # Add Summary Fields
        summaryCardLayout.addWidget(
            self._createSummaryCardField("Number of Aggregations Composed", self._userSummary["aggregationCount"]))
        summaryCardLayout.addWidget(
            self._createSummaryCardField("Number of Articles Added", self._userSummary["articleCount"]))
        summaryCardLayout.addWidget(
            self._createSummaryCardField("Number of Ratings/Comments Posted", self._userSummary["ratingCount"]))

        summaryCard.setLayout(summaryCardLayout)
        return summaryCard

    def _createStatsPlot(self) -> QWidget:
        statsCard = QWidget()                   # Initialize Stats Plot Widget
        statsCardLayout = QVBoxLayout()         # Initialize Stats Plot Layout

        # Apply Styles
        statsCard.setCursor(QCursor(Qt.PointingHandCursor))
        statsCard.setObjectName("statisticsCard")
        statsCard.setStyleSheet(
            """
                QWidget#statisticsCard { background: white; border: 1px solid gainsboro; border-radius: 2px; text-align: center; }
                QWidget#statisticsCard:hover { border: 1px solid grey; }
            """
        )

        statsCardLayout.addWidget(QLabel("<h2>Usage Statistics.</h2>"))             # Set Header
        statsCardLayout.addWidget(self._createSeparator())                          # Insert Separator
        statsCardLayout.addWidget(QLabel("&nbsp;&nbsp;&nbsp;<h4>Time Spent</h4>"))  # Set Subheader

        # Create Data Plot
        timeSpent = PlotWidget()
        for a in self._aggregations:
            aggregation_day = parser.parse(a["timestamp"]).day              # Parse Day from aggregation timestamp
            delta = aggregation_day - datetime.now().day                    # Parse Day from current timestamp
            timeSpent.plot([delta], [1], pen=mkPen(color=(255, 0, 0)))      # Plot point
        timeSpent.setBackground("w")            # Set background to white
        statsCardLayout.addWidget(timeSpent)

        statsCard.setLayout(statsCardLayout)    # Set Stats Card Layout
        return statsCard

    def _createIdBar(self) -> QWidget:
        profileId = QWidget()               # Initialize Profile Id Widget
        profileIdLayout = QHBoxLayout()     # Initialize Profile Id Layout

        # Create User Icon
        userIcon = qta.icon("fa5s.user", color="black")
        userIconWidget = QLabel()
        userIconWidget.setPixmap(userIcon.pixmap(48, 48))
        userIconWidget.mouseReleaseEvent = self._openProfileDialogTrigger
        profileIdLayout.addWidget(userIconWidget, 1)

        # Create User Banner
        userBanner = QLabel(f"<h1>{self._username}</h1>")
        userBanner.mouseReleaseEvent = self._openProfileDialogTrigger
        profileIdLayout.addWidget(userBanner, 5, alignment=Qt.AlignLeft)

        # Set Profile Id Layout
        profileId.setLayout(profileIdLayout)

        return profileId

    def _createSeparator(self) -> QFrame:
        separator = QFrame()
        separator.setFrameShape(QFrame.HLine)
        separator.setStyleSheet("QFrame { color: gainsboro; }")
        return separator

    def _createBio(self) -> QLabel:
        bio = QLabel(f"<p>{self._bio}</p>")
        bio.setWordWrap(True)
        bio.mouseReleaseEvent = self._openProfileDialogTrigger
        return bio

    def _createSummaryCardField(self, label, value):
        field = QWidget()
        fieldLayout = QHBoxLayout()
        fieldLayout.addWidget(QLabel(label))
        valueWidget = QLabel(str(value))
        fieldLayout.addWidget(valueWidget, alignment=Qt.AlignRight)
        # fieldLayout.setAlignment(valueWidget, Qt.AlignRight)
        field.setLayout(fieldLayout)
        return field

    def _getUserSummary(self, userId: int):
        res = requests.get(f"{Config.API_BASE_ENDPOINT}/getUserSummary/{userId}", json = {
            "auth_token": self._auth_token
        })
        deserialized_res = res.json()
        userSummary = {
            "aggregationCount": deserialized_res["aggregation_count"],
            "articleCount": deserialized_res["article_count"],
            "ratingCount": deserialized_res["rating_count"]
        }
        return userSummary

    def _getUserDetails(self, userId: int) -> str:
        res = requests.get(f"{Config.API_BASE_ENDPOINT}/getUser/{userId}", json = {
            "auth_token": self._auth_token
        })
        deserialized_res = res.json()
        user = deserialized_res["user"]
        return user["bio"]

    def _getUserAggregations(self, userId: int):
        res = requests.get(f"{Config.API_BASE_ENDPOINT}/getAggregationsByUser/{self._user_id}", json={
            "auth_token": self._auth_token
        })
        deserialized_res = res.json()
        print(deserialized_res)
        return deserialized_res["aggregations"]

    def _retrieveProfileData(self) -> None:
        self._userSummary = self._getUserSummary(self._user_id)
        self._bio = self._getUserDetails(self._user_id)
        self._aggregations = self._getUserAggregations(self._user_id)

