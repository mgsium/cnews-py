from PyQt5.QtCore import Qt, pyqtSlot
from PyQt5.QtGui import QIcon, QCursor
from PyQt5.QtWidgets import QDialog, QVBoxLayout, QLabel, QLineEdit, QPushButton, QFrame

from components.Pages.ArticlePreviewDialog import ArticlePreviewDialog
from components.Pages.FeaturedArticleBrowser import FeaturedArticleBrowser

class AddArticleDialog(QDialog):
    """Dialog for searching for articles."""

    def __init__(self, active_aggregation_id, auth_token, refresh_article_table):
        super().__init__()

        self.setWindowTitle("Add an Article")
        self.setWindowIcon(QIcon("assets\img\png\cn_icon.png"))
        self.setFixedWidth(450)
        self._auth_token = auth_token

        self._active_aggregation_id = active_aggregation_id
        self._refresh_article_table = refresh_article_table

        layout = QVBoxLayout()
        layout = self._setDialogHeader(layout)
        layout = self._createDialogBody(layout)

        self.setLayout(layout)
        self.show()


    def _setDialogHeader(self, layout):
        header = QLabel("<h2>Add an Article</h2>")
        layout.addWidget(header)
        return layout

    def _createDialogBody(self, layout):

        # Url Input Box
        self.urlInputBox = QLineEdit()
        self.urlInputBox.setPlaceholderText("Enter Article URL...")
        layout.addWidget(self.urlInputBox)

        # Submit Url Btn
        self.urlSubmitBtn = QPushButton("Retrieve Article")
        self.urlSubmitBtn.setCursor(Qt.PointingHandCursor)
        self.urlSubmitBtn.mouseReleaseEvent = self._openArticlePreviewDialog
        layout.addWidget(self.urlSubmitBtn)

        # Dividing Line
        horizontalDividingLine = QFrame()
        horizontalDividingLine.setFrameShape(QFrame.HLine)
        horizontalDividingLine.setFrameShadow(QFrame.Sunken)
        layout.addWidget(horizontalDividingLine)

        # Featured Button
        featuredBtn = QPushButton("Browse Featured")
        featuredBtn.setStyleSheet("""
            QPushButton {
                background-color: black;
                color: white;
                font-size: 14pt;
                padding: 20px;
            }
        """)
        featuredBtn.mouseReleaseEvent = self._openFeaturedDialog
        featuredBtn.setCursor(QCursor(Qt.PointingHandCursor))
        layout.addWidget(featuredBtn)

        return layout

    @pyqtSlot()
    def _openFeaturedDialog(self, event):
        featuredDialog = FeaturedArticleBrowser(self._active_aggregation_id, self._auth_token, \
                                                self._refresh_article_table)
        featuredDialog.exec()

    def _openArticlePreviewDialog(self, event):
        url = self.urlInputBox.text()

        # Use regex test
        if (len(url) == 0) :
            # TODO: Open Error Box Here
            return

        articlePreviewDialog = ArticlePreviewDialog(self._active_aggregation_id, url, self._auth_token, \
                                                    self._refresh_article_table)
        articlePreviewDialog.exec()