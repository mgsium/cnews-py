from typing import Tuple

import requests
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QDialog, QVBoxLayout, QLabel, QLineEdit, QPlainTextEdit, QGroupBox, QPushButton

from config import Config


class ProfileDialog(QDialog):
    """Dialog for Viewing and Editing Account details."""

    __slots__ = ("_user_id", "_auth_token", "_username", "_bio", "_password_hash")

    def __init__(self, user_id, auth_token):
        super().__init__()

        self.setWindowTitle("Account Details")
        self.setWindowIcon(QIcon("assets\img\png\cn_icon.png"))
        self.setFixedSize(450, 600)

        self._user_id = user_id
        self._auth_token = auth_token
        self._retrieveProfileData()

        layout = QVBoxLayout()
        self._save_btn = self._createSaveBtn()
        layout = self._setDialogHeader(layout)
        layout = self._createUsernameField(layout)
        layout = self._createBioField(layout)
        layout = self._createPasswordField(layout)
        layout.addWidget(self._save_btn)

        self.setLayout(layout)
        self.show()

    def _setDialogHeader(self, layout: QVBoxLayout) -> QVBoxLayout:
        layout.addWidget(QLabel("<h2>Account Details</h2>"))
        layout.setAlignment(Qt.AlignCenter)
        return layout

    def _createUsernameField(self, layout: QVBoxLayout) -> QVBoxLayout:
        username_group_box = QGroupBox("Username")
        username_group_box_layout = QVBoxLayout()
        username_group_box_layout.addWidget(QLabel("<h4>Username</h4>"))
        username_line_edit = QLineEdit()
        username_line_edit.setText(self._username)
        username_group_box_layout.addWidget(username_line_edit)
        username_group_box.setLayout(username_group_box_layout)
        layout.addWidget(username_group_box)
        return layout

    def _createBioField(self, layout: QVBoxLayout) -> QVBoxLayout:
        bio_group_box = QGroupBox("Bio")
        bio_group_box_layout = QVBoxLayout()
        bio_group_box_layout.addWidget(QLabel("<h4>Your Bio</h4>"))
        bio_field = QPlainTextEdit()
        bio_field.setPlainText(self._bio)
        bio_group_box_layout.addWidget(bio_field)
        bio_group_box.setLayout(bio_group_box_layout)
        layout.addWidget(bio_group_box)
        return layout

    def _createPasswordField(self, layout: QVBoxLayout) -> QVBoxLayout:
        password_group_box = QGroupBox("Password")
        password_group_box_layout = QVBoxLayout()
        password_group_box_layout.addWidget(QLabel("<h4>Change your Password</h4>"))
        password_line_edit = QLineEdit()
        password_group_box_layout.addWidget(password_line_edit)
        password_group_box_layout.addWidget(QLabel("<h4>Confirm New Password</h4>"))
        confirm_password_line_edit = QLineEdit()
        password_group_box_layout.addWidget(confirm_password_line_edit)
        password_group_box.setLayout(password_group_box_layout)
        layout.addWidget(password_group_box)
        return layout

    def _createSaveBtn(self) -> QPushButton:
        save_btn = QPushButton("Save Details")
        save_btn.setStyleSheet("""
            QPushButton {
                background-color: black;
                color: white;
                font-size: 14pt;
                padding: 20px;
            }
        """)
        save_btn.setCursor(Qt.PointingHandCursor)
        return save_btn

    def _getUserDetails(self, user_id) -> Tuple[str, str, str]:
        res = requests.get(f"{Config.API_BASE_ENDPOINT}/getUser/{user_id}", json={
            "auth_token": self._auth_token
        })
        deserialized_res = res.json()
        user = deserialized_res["user"]
        return user["username"], user["bio"], user["password_hash"]

    def _retrieveProfileData(self):
        self._username, self._bio, self._password_hash = self._getUserDetails(self._user_id)
