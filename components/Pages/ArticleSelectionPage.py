import requests
from PyQt5.QtCore import Qt, pyqtSlot
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QSplitter, QFrame, QVBoxLayout, QTableWidget, QLabel, QHBoxLayout, QPushButton, QGroupBox, \
    QTableWidgetItem, QHeaderView

from components.Common import CommonEffects
from components.Pages.AddArticleDialog import AddArticleDialog
from components.Pages.ArticleReader import ArticleReader
from config import Config

class ArticleSelectionPage:
    """A page to select articles to add to a newly created aggrgegation."""

    def __init__(self, user_id, auth_token, switch_to_page):
        super()
        self._user_id = user_id
        self._auth_token = auth_token
        self._switch_to_page = switch_to_page

    def _createPage(self) -> QSplitter:
        # Create Upper and Lower Frames
        viewFrame = self._getViewFrame()
        controlFrame = self._getControlFrame()

        # Create Splitter
        splitter = QSplitter(Qt.Vertical)
        splitter.addWidget(viewFrame)
        splitter.addWidget(controlFrame)

        splitter.setStretchFactor(2.5, 1)
        splitter.setHandleWidth(3)  # Set Splitter handle width to 3px
        splitter.setStyleSheet("QSplitter { background: white }") # Set Background color within splitter.

        return splitter

    def _getViewFrame(self) -> QFrame:
        # Initialize View Frame
        viewFrame = QFrame()
        viewFrame.setFrameShape(QFrame.StyledPanel)

        # Initialize Frame Layout
        boxLayout = QVBoxLayout()

        # Create Aggregation Name Header
        self._nameHeader = QLabel("Aggregation not Initialized")
        self._nameHeader.setAlignment(Qt.AlignCenter)
        self._nameHeader.setStyleSheet("QLabel { font-size: 18pt }")
        boxLayout.addWidget(self._nameHeader)

        # Populate Frame Layout
        self._articleTableWidget = QTableWidget()
        self._articleTableWidget.setColumnCount(5)
        self._articleTableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Fixed)
        self._articleTableWidget.setRowCount(0)
        self._articleTableWidget.setHorizontalHeaderLabels([
            "Article ID",
            "Header",
            "No. of Words",
            "Read",
            "Delete"
        ])
        

        self._articleTableWidget.resizeColumnsToContents()
        self._articleTableWidget.setColumnWidth(1, 500)
        self._articleTableWidget.setColumnWidth(3, 175)
        self._articleTableWidget.horizontalHeader().setStretchLastSection(True)
        self._articleTableWidget.verticalHeader().hide()
        boxLayout.addWidget(self._articleTableWidget)

        # Set Frame Layout
        viewFrame.setLayout(boxLayout)

        return viewFrame


    def _getControlFrame(self) -> QFrame:
        # Initialize Control Frame
        controlFrame = QFrame()
        controlFrame.setFrameShape(QFrame.StyledPanel)
        controlFrame.setMaximumHeight(300)
        controlFrameInner = QFrame()
        controlFrameInner.setMaximumWidth(1000)

        # Initialize Frame Layout
        controlFrameLayout = QVBoxLayout()
        controlFrameInnerLayout = QHBoxLayout()

        # Populate Frame Layout
        lowerGraphic = QPixmap("assets\img\png\\read_the_world.png")
        lowerGraphic = lowerGraphic.scaledToWidth(500)
        lowerGraphicWidget = QLabel()
        lowerGraphicWidget.setPixmap(lowerGraphic)
        controlBoxLayout = QVBoxLayout()

        # Add Article Button
        addArticleButton = QPushButton("Add Article")
        addArticleButton.mouseReleaseEvent = self._openAddArticleDialog
        addArticleButton.setStyleSheet("""
            QPushButton { 
                background-color: #007bff; 
                color: white; 
                font-size: 18pt; 
                border-radius: 8px; 
                padding: 10px;     
            }
        """)
        addArticleButton.setCursor(Qt.PointingHandCursor)
        addArticleButton.setFixedHeight(110)
        controlBoxLayout.addWidget(addArticleButton, alignment=Qt.AlignTop)

        # Finish Button
        finishButton = QPushButton("Finish")
        finishButton.setStyleSheet("""
            QPushButton {
                background-color: #EF233C; 
                color: white; 
                font-size: 18pt;
                border-radius: 8px;
                padding: 10px;
            }
        """)
        finishButton.setCursor(Qt.PointingHandCursor)
        finishButton.clicked.connect(lambda:self._switch_to_page(3))
        finishButton.setFixedHeight(110)
        controlBoxLayout.addWidget(finishButton)

        # Initializing the control box widget
        controlBox = QGroupBox()
        controlBox.setObjectName("controlBox")
        controlBox.setStyleSheet(
            "QGroupBox#controlBox { background-color: whitesmoke; border-radius: 8px; border: 1px solid gainsboro; }")
        controlBox.setGraphicsEffect(CommonEffects.createGroupBoxShadow())
        controlBox.setFixedHeight(250)
        controlBox.setMinimumWidth(475)
        controlBox.setLayout(controlBoxLayout)
        controlFrameInnerLayout.addWidget(lowerGraphicWidget)
        controlFrameInnerLayout.addWidget(controlBox)

        # Set Frame Layout
        controlFrameInner.setLayout(controlFrameInnerLayout)
        controlFrameLayout.addWidget(controlFrameInner, alignment=Qt.AlignCenter)
        controlFrame.setLayout(controlFrameLayout)

        return controlFrame

    def _setActiveAggregation(self, aggregation_id, aggregation_name) -> None:
        self._active_aggregation_id = int(aggregation_id)
        self._active_aggregation_name = aggregation_name
        self._refreshPage()

    def _refreshPage(self):
        # Update Aggregation Name Header
        self._nameHeader.setText(self._active_aggregation_name)

        res = requests.get(f"{Config.API_BASE_ENDPOINT}/getArticlesByAggregation/{self._active_aggregation_id}")
        deserialized_res = res.json()
        articles = deserialized_res["articles"]

        self._articleTableWidget.setRowCount(len(articles))
        row = 0
        for a in articles:
            # Create Table Widget
            article_id = QTableWidgetItem(f"{a['article_id']}")
            article_id.setTextAlignment(Qt.AlignCenter)
            self._articleTableWidget.setItem(row, 0, article_id)

            # Add Article Header Fied
            article_header = QTableWidgetItem(a['header'])
            article_header.setTextAlignment(Qt.AlignCenter)
            self._articleTableWidget.setItem(row, 1, article_header)

            # Add Word Count Field
            word_count = QTableWidgetItem(f"{len(a['body'].split(' '))}")
            word_count.setTextAlignment(Qt.AlignCenter)
            self._articleTableWidget.setItem(row, 2, word_count)

            for table_item in [article_id, article_header, word_count]:
                table_item.setFlags(table_item.flags() ^ (Qt.ItemIsSelectable | Qt.ItemIsEditable))

            # Add Read Btn
            read_btn = QPushButton("  Read  ")
            read_btn.setCursor(Qt.PointingHandCursor)
            read_btn.setStyleSheet("""
                            QPushButton {
                                background-color: #4285F4;
                                color: white;
                                border: 0px;
                            }
                        """)
            read_btn.clicked.connect(self._getReadMethod(a["header"], a["body"]))
            self._articleTableWidget.setCellWidget(row, 3, read_btn)

            # Add Delete Button
            del_btn = QPushButton("  Delete  ")
            del_btn.setCursor(Qt.PointingHandCursor)
            del_btn.setStyleSheet("""
                            QPushButton {
                                background-color: #EF233C;
                                color: white;
                                border: 0px;
                            }
                        """)
            del_btn.clicked.connect(self._getDelMethod(a["article_id"], row))
            self._articleTableWidget.setCellWidget(row, 4, del_btn)

            row += 1

    @pyqtSlot()
    def _openAddArticleDialog(self, event):
        addArticleDialog = AddArticleDialog(self._active_aggregation_id, self._auth_token, self._refreshPage)
        addArticleDialog.exec()

    # Closure which returns a higher-order delete function given an article id
    def _getDelMethod(self, id: int, row: int):
        def delete():
            res = requests.delete(f"{Config.API_BASE_ENDPOINT}/removeArticle/{id}", json={
                "auth_token": self._auth_token
            })
            deserialized_res = res.json()
            self._articleTableWidget.removeRow(row)

        return delete

    # Closure which returns a high-order read function (open reader) given an article header & body, and aggregation name
    def _getReadMethod(self, article_header, article_body):
        def edit():
            articleReader = ArticleReader(self._active_aggregation_name, article_header, article_body)
            articleReader.exec()

        return edit
