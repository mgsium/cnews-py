import requests
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QDialog, QVBoxLayout, QLabel, QComboBox, QListWidget

from components.Pages.ArticlePreviewDialog import ArticlePreviewDialog
from config import Config


class FeaturedArticleBrowser(QDialog):
    """Dialog for discovering featured articles."""

    filter_options = ["All", "BBC", "Sky News", "CNN", "Fox News"]

    def __init__(self, active_aggregation_id, auth_token, refresh_article_table):
        super().__init__()

        self.setWindowTitle("Featured Articles")
        self.setWindowIcon(QIcon("assets\img\png\cn_icon.png"))
        self.setFixedWidth(500)
        self.setMinimumHeight(600)

        self._active_aggregation_id = active_aggregation_id
        self._auth_token = auth_token
        self._refresh_article_table = refresh_article_table
        self._articles = self._retrieveArticles()

        layout = QVBoxLayout()

        layout = self._createDialogHeader(layout)
        layout = self._createFilterSelection(layout)
        layout = self._createArticleList(layout)

        self.setLayout(layout)


    def _createDialogHeader(self, layout):
        header = QLabel("<h2>Featured Articles</h2>")
        layout.addWidget(header)
        return layout

    def _createFilterSelection(self, layout):
        filterSelect = QComboBox()
        filterSelect.addItems(self.filter_options)
        filterSelect.currentIndexChanged.connect(self._updateFilter)
        layout.addWidget(filterSelect)
        return layout

    def _createArticleList(self, layout):
        # Create List Widget
        self._articleList = QListWidget()
        self._articleList.setWordWrap(True)
        self._articleList.setCursor(Qt.PointingHandCursor)
        self._articleList.itemClicked.connect(self._addArticle)
        # Add Items to list
        # self._articleList.addItems([v["header"] for v in self._articles["fox"]])
        self._applyFilter("all")
        # Add Article List Widget to dialog layout
        layout.addWidget(self._articleList)
        return layout

    def _addArticle(self, entry):
        entry_text = entry.data(0)
        header, source = entry_text.split("\n")
        source = source.replace("From", "").strip().lower()
        article_group = self._articles[source]
        headers = [a["header"] for a in article_group]
        link = article_group[headers.index(header)]["link"]
        self._openArticlePreviewDialog(link)


    def _applyFilter(self, filter: str):
        self._articleList.clear()
        entry_template = lambda header, source: f"{header}\nFrom {source.upper() if source in ['bbc', 'cnn'] else source.capitalize()}"
        for key in self._articles.keys():
            if key == filter or filter == "all":
                self._articleList.addItems([entry_template(v["header"], key) for v in self._articles[key]])
        self._articleList.sortItems(Qt.AscendingOrder)

    def _updateFilter(self, index: int):
        choice = self.filter_options[index]
        choice = choice.replace("News", "").strip().lower()
        self._applyFilter(choice)

    def _retrieveArticles(self):
        res = requests.get(f"{Config.API_BASE_ENDPOINT}/getFeaturedArticles")
        deserialized_res = res.json()
        # TODO: Handle Bad Response
        articles = deserialized_res["data"]["articles"]
        return articles

    def _openArticlePreviewDialog(self, article_url):
        articlePreviewDialog = ArticlePreviewDialog(self._active_aggregation_id, article_url, self._auth_token, \
                                                    self._refresh_article_table)
        articlePreviewDialog.exec()

