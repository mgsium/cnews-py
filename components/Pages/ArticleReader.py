from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QVBoxLayout, QDialog, QLabel, QScrollArea, QFrame, QPushButton


class ArticleReader(QDialog):
    """Dialog for reading a saved article."""

    def __init__(self, aggregation_name, header, body):
        super().__init__()

        self.setWindowTitle(f"Reading {header}")
        self.setWindowIcon(QIcon("assets\img\png\cn_icon.png"))
        self.setFixedWidth(640)
        self.setMaximumHeight(750)

        layout = QVBoxLayout()
        layout = self._setDialogHeader(layout, header)
        layout = self._setDialogSubheader(layout, aggregation_name)
        layout = self._insertHorizontalLine(layout)
        layout = self._insertArticleContents(layout, body)
        layout = self._insertExitBtn(layout)

        self.setLayout(layout)
        self.show()

    def _setDialogHeader(self, layout: QVBoxLayout, article_header: str) -> QVBoxLayout:
        header = QLabel(f"<h2>{article_header}</h2>")
        header.setStyleSheet("QLabel { font-weight: bold }")
        header.setWordWrap(True)
        header.setAlignment(Qt.AlignCenter)
        layout.addWidget(header)
        return layout

    def _setDialogSubheader(self, layout: QVBoxLayout, aggregation_name: str) -> QVBoxLayout:
        subheader = QLabel(f"Part of {aggregation_name}")
        subheader.setObjectName("subheader")
        subheader.setStyleSheet("QLabel#subheader { color: #666 }")
        subheader.setAlignment(Qt.AlignCenter)
        layout.addWidget(subheader)
        return layout

    def _insertHorizontalLine(self, layout: QVBoxLayout) -> QVBoxLayout:
        horizontalDividingLine = QFrame()
        horizontalDividingLine.setFrameShape(QFrame.HLine)
        horizontalDividingLine.setFrameShadow(QFrame.Sunken)
        horizontalDividingLine.setContentsMargins(0, 10, 0, 10)
        layout.addWidget(horizontalDividingLine)

        return layout

    def _insertArticleContents(self, layout: QVBoxLayout, article_text: str) -> QVBoxLayout:
        scrollArea = QScrollArea()
        scrollArea.setFixedWidth(620)
        articleTextWidget = QLabel(article_text)
        articleTextWidget.setMaximumWidth(scrollArea.width() - 20)
        articleTextWidget.setStyleSheet("QLabel { padding: 10px; background: white; }")
        articleTextWidget.setWordWrap(True)
        scrollArea.setWidget(articleTextWidget)
        layout.addWidget(scrollArea)
        return layout

    def _insertExitBtn(self, layout: QVBoxLayout) -> QVBoxLayout:
        exit_btn = QPushButton("Close Article")
        exit_btn.setCursor(Qt.PointingHandCursor)
        exit_btn.setStyleSheet("""
            QPushButton {
                background-color: black;
                color: white;
                font-size: 14pt;
                padding: 20px;
            }
        """)
        exit_btn.clicked.connect(lambda:self.close())
        layout.addWidget(exit_btn)
        return layout
