import requests
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QLabel, QGridLayout, QTableWidget, QTableWidgetItem, QAbstractScrollArea, QVBoxLayout, \
    QPushButton, QHeaderView

from config import Config


class ArchivePage:
    """Page to view, edit and delete existing aggregations."""

    def __init__(self, user_id, auth_token, set_active_aggregation, switch_to_page):
        super()

        self._auth_token = auth_token
        self._user_id = user_id
        self._set_active_aggregation = set_active_aggregation
        self._switch_to_page = switch_to_page

        self._getAggregations()

    def _createArchive(self) -> QLabel:
        archive = QLabel()
        archiveLayout = QVBoxLayout()

        # Page Header
        header = QLabel("<h1>Archive</h1>")
        header.setAlignment(Qt.AlignCenter)
        archiveLayout.addWidget(header)

        # Table of Aggregations
        self._tableWidget = QTableWidget()
        self._tableWidget.setColumnCount(6)
        self._tableWidget.setRowCount(len(self._aggregations))
        self._tableWidget.horizontalHeader().setStretchLastSection(True)                  # Fit Columns to container
        self._tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Fixed)      # Disable Column Resize
        self._tableWidget.hideColumn(3)                                                   # Hide User ID Column
        self._tableWidget.setHorizontalHeaderLabels([
            "Aggregation ID",
            "Name",
            "Time Created",
            "User ID",
            "Open",
            "Delete"
        ])

        self._populateTable()

        self._tableWidget.resizeColumnsToContents()
        self._tableWidget.setColumnWidth(1, 400)
        self._tableWidget.setColumnWidth(2, 280)
        self._tableWidget.setColumnWidth(4, 80)
        self._tableWidget.verticalHeader().hide()

        archiveLayout.addWidget(self._tableWidget)

        archive.setLayout(archiveLayout)
        return archive

    def refreshPage(self):
        self._getAggregations()
        self._populateTable()

    def _populateTable(self):
        self._tableWidget.clear()                                   # Clear Entries
        self._tableWidget.setRowCount(len(self._aggregations))      # Reset Row Count

        # Set Column Headers
        self._tableWidget.setHorizontalHeaderLabels([
            "Aggregation ID",
            "Name",
            "Time Created",
            "User ID",
            "Open",
            "Delete"
        ])

        # Populating the Table
        y = 0
        for a in self._aggregations:
            x = 0
            a_values = list(a.values())
            while (x < len(a_values)):
                table_item = QTableWidgetItem(str(a_values[x]))
                table_item.setTextAlignment(Qt.AlignCenter)
                # Set Cells to Read-Only mode
                table_item.setFlags(table_item.flags() ^ (Qt.ItemIsSelectable | Qt.ItemIsEditable))
                if x == 1:
                    table_item.setToolTip("Read Aggregation")
                self._tableWidget.setItem(y, x, table_item)
                x += 1

            # Add Open Btn
            open_btn = QPushButton("  Open  ")
            open_btn.setCursor(Qt.PointingHandCursor)
            open_btn.setStyleSheet("""
                        QPushButton {
                            background-color: #4285F4;
                            color: white;
                            border: 0px;
                        }
                    """)
            open_btn.clicked.connect(self._getEditMethod(a_values[0], a_values[1]))
            self._tableWidget.setCellWidget(y, x, open_btn)

            # Add Delete Button
            del_btn = QPushButton("  Delete  ")
            del_btn.setCursor(Qt.PointingHandCursor)
            del_btn.setStyleSheet("""
                        QPushButton {
                            background-color: #EF233C;
                            color: white;
                            border: 0px;
                        }
                    """)
            del_btn.clicked.connect(self._getDelMethod(a_values[0], y))
            self._tableWidget.setCellWidget(y, x + 1, del_btn)

            y += 1

    def _getAggregations(self):
        res = requests.get(f"{Config.API_BASE_ENDPOINT}/getAggregationsByUser/{self._user_id}", json={
            "auth_token": self._auth_token
        })
        deserialized_res = res.json()
        self._aggregations = deserialized_res["aggregations"]

    # Closure which returns a higher-order delete function given an aggregation id
    def _getDelMethod(self, id: int, row: int):

        def delete():
            res = requests.delete(f"{Config.API_BASE_ENDPOINT}/removeAggregation/{id}", json={
                "auth_token": self._auth_token
            })
            deserialized_res = res.json()
            self._tableWidget.removeRow(row)

        return delete

    # Closure which returns a high-order edit function given an aggregation id & name
    def _getEditMethod(self, id, name):

        def edit():
            self._set_active_aggregation(id, name)
            self._switch_to_page(4)

        return edit
