import webbrowser

from PyQt5 import QtCore
from PyQt5.QtCore import QSize, Qt
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QGridLayout, QWidget, QVBoxLayout, QLabel
import qtawesome as qta

from config import Config


class HomePage:

    def __init__(self, user_id, auth_token, switch_to_page):
        super()
        self._user_id = user_id
        self._auth_token = auth_token
        self._switch_to_page = switch_to_page

    def _createPage(self):
        homeLayout = QGridLayout()
        homeLayout = self._createUserCard(homeLayout)
        homeLayout = self._createQuickLinksBox(homeLayout)
        homeLayout = self._createWelcomeCard(homeLayout)
        homeLayout = self._createApiGraphic(homeLayout)
        homeLayout = self._createPlatformExtensionCard(homeLayout)
        return homeLayout

    def _createUserCard(self, layout: QGridLayout) -> QGridLayout:
        # Create User Card Widget
        userCard = QWidget()
        userCard.setToolTip("Go to User Page")
        userCard.setCursor(Qt.PointingHandCursor)
        userCard.mouseReleaseEvent = self._goToProfilePage # Go to Profile Page on Click
        userCard.setObjectName("userCardHome")
        userCard.setStyleSheet(
            """
                QWidget#userCardHome { background: #4285F4; border: 1px solid gainsboro; border-radius: 2px; }
                QWidget#userCardHome:hover { border: 1px solid grey; }
            """
        )

        # Create User Card Layout
        userCardLayout = QVBoxLayout()

        # Add User Icon
        userIcon = qta.icon("fa5s.user", color="white")
        userIconWidget = QLabel()
        userIconWidget.setPixmap(userIcon.pixmap(80, 80))
        userIconWidget.setAlignment(Qt.AlignCenter | Qt.AlignBottom)
        userCardLayout.addWidget(userIconWidget)

        # Add Prompt
        cardPrompt = QLabel("Profile")
        cardPrompt.setAlignment(Qt.AlignCenter | Qt.AlignTop)
        cardPrompt.setStyleSheet("QLabel { color: white }")
        userCardLayout.addWidget(cardPrompt)

        userCard.setLayout(userCardLayout)

        # Add user card to Page Layout
        layout.addWidget(userCard, 1, 1)

        return layout

    def _createQuickLinksBox(self, layout: QGridLayout) -> QGridLayout:
        # Create Quick Links Widget
        quickLinks = QWidget()
        quickLinks.setObjectName("quickLinksBox")
        quickLinks.setStyleSheet(
            """
                QWidget#quickLinksBox { background: white; border: 1px solid gainsboro; border-radius: 2px; }
                QWidget#quickLinksBox:hover { border: 1px solid grey; }
            """
        )

        # Create Quick Links Layout
        quickLinksLayout = QVBoxLayout()

        # Header Widget
        header = QLabel("<h2>Quick Links</h2>")
        header.setStyleSheet("QLabel { font-weight: bold }")
        header.setAlignment(Qt.AlignCenter)
        quickLinksLayout.addWidget(header)

        # Link in the format [ LINK_TEXT, URI ]
        links = [
            ["Create your first aggregation", "https://nmspace.org/guide?guide_id=1649"],
            ["Central News Wiki", "https://gitlab.com/mgsium/cnews-py/-/wikis/home"]
        ]

        for l in links:
            linkWidget = QLabel(f"<a href='#'>{l[0]}</a>")
            linkWidget.mouseReleaseEvent = lambda e: webbrowser.open(l[1])
            linkWidget.setAlignment(Qt.AlignCenter)
            quickLinksLayout.addWidget(linkWidget)

        quickLinks.setLayout(quickLinksLayout)

        # Add Quick Links Box to Page Layout
        layout.addWidget(quickLinks, 1, 0)

        return layout

    def _createWelcomeCard(self, layout: QGridLayout) -> QGridLayout:
        # Create Welcome Card Widget
        welcomeCard = QWidget()
        welcomeCard.setObjectName("welcomeCard")
        welcomeCard.setStyleSheet(
            """
                QWidget#welcomeCard { background: white; border: 1px solid gainsboro; border-radius: 2px; }
                QWidget#welcomeCard:hover { border: 1px solid grey; }
            """
        )

        # Create Welcome Card Layout
        welcomeCardLayout = QVBoxLayout()

        # Header Widget
        header = QLabel("<h2>Welcome</h2>")
        header.setStyleSheet("QLabel { font-weight: bold }")
        header.setAlignment(Qt.AlignCenter)
        welcomeCardLayout.addWidget(header)

        # Text Widget
        body = QLabel("Central News is an Aggregation platform designed to encourage balanced viewpoints when exploring news sources. This application allows you to create, save and re-read aggregations of articles from multiple sources.")
        body.setAlignment(Qt.AlignCenter | Qt.AlignTop)
        body.setWordWrap(True)
        welcomeCardLayout.addWidget(body)

        welcomeCard.setLayout(welcomeCardLayout)

        # Add Welcome Card to Page Layout
        layout.addWidget(welcomeCard, 0, 0, 1, 2)

        return layout

    def _createApiGraphic(self, layout: QGridLayout) -> QGridLayout:
        # Create API Graphic Pixmap
        self._apiGraphicPixmap = QPixmap("assets\img\png\\api_graphic.png")
        # TODO: Set height & width to expand proportionally

        # Create API Graphic Widget
        self._apiGraphicWidget = QLabel()
        self._apiGraphicWidget.setMinimumSize(320, 320)
        self._apiGraphicWidget.resizeEvent = self._apiImgResizeHandler
        self._apiGraphicWidget.mouseReleaseEvent = lambda e: webbrowser.open(Config.GITLAB_BE_REPO_LINK)
        self._apiGraphicWidget.setCursor(Qt.PointingHandCursor)
        self._apiGraphicWidget.setToolTip("Go to GitLab")
        self._apiGraphicWidget.setPixmap(self._apiGraphicPixmap)

        # Add API Graphic to Page Layout
        layout.addWidget(self._apiGraphicWidget, 0, 2)

        return layout

    # TODO: Implement Remaining Home Page Widgets
    def _createPlatformExtensionCard(self, layout: QGridLayout) -> QGridLayout:
        # Create Platform Extension Widget
        platformExtCard = QWidget()
        platformExtCard.setObjectName("platformExtCard")
        platformExtCard.setMaximumWidth(500)
        platformExtCard.setStyleSheet(
            """
                QWidget#platformExtCard { background: white; border: 1px solid gainsboro; border-radius: 2px; }
                QWidget#platformExtCard:hover { border: 1px solid grey; }
            """
        )

        # Create Platform Ext Card Layout
        platformExtCardLayout = QVBoxLayout()

        # Header Widget
        header = QLabel("<h2>Extending the Platform</h2>")
        header.setStyleSheet("QLabel { font-weight: bold }")
        header.setAlignment(Qt.AlignCenter)
        platformExtCardLayout.addWidget(header)

        # Text Widget
        body = QLabel("The Central News API is Open Source & Extensible. Click the Graphic above to view the GitLab Repository")
        body.setAlignment(Qt.AlignCenter | Qt.AlignTop)
        body.setWordWrap(True)
        platformExtCardLayout.addWidget(body)

        platformExtCard.setLayout(platformExtCardLayout)

        # Add Platform Ext Card to Page Layout
        layout.addWidget(platformExtCard, 1, 2)

        return layout

    def _apiImgResizeHandler(self, event):
        w = min(self._apiGraphicWidget.width(), self._apiGraphicPixmap.width())
        h = min(self._apiGraphicWidget.height(), self._apiGraphicPixmap.height())
        self._apiGraphicWidget.setPixmap(self._apiGraphicPixmap.scaled(w, h, QtCore.Qt.KeepAspectRatio))

    def _goToProfilePage(self, event):
        self._switch_to_page(1)