import requests
from PyQt5.QtCore import Qt, pyqtSlot
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QFrame, QSplitter, QVBoxLayout, QLabel, QHBoxLayout, QLineEdit, QGroupBox, QPushButton
import qtawesome as qta

from components.Common import CommonEffects
from config import Config


class NewAggregationPage:
    """A page to create new aggregations."""

    def __init__(self, user_id, auth_token, set_active_aggregation, switch_to_page):
        super()
        self._user_id = user_id
        self._auth_token = auth_token
        self._set_active_aggregation = set_active_aggregation
        self._switch_to_page = switch_to_page

    def _createPage(self) -> QSplitter:
        # Create Upper and Lower Frames
        viewFrame = self._getViewFrame()
        controlFrame = self._getControlFrame()

        # Create Splitter
        splitter = QSplitter(Qt.Vertical)
        splitter.addWidget(viewFrame)
        splitter.addWidget(controlFrame)

        splitter.setStretchFactor(2.5, 1)
        splitter.setHandleWidth(3)  # Set Splitter Handle Width to 3px
        splitter.setStyleSheet("QSplitter { background: white }")  # Set Background color within splitter

        return splitter

    def _getViewFrame(self) -> QFrame:
        # Initialize View Frame
        viewFrame = QFrame()
        viewFrame.setFrameShape(QFrame.StyledPanel)

        # Initialize Frame Layout
        boxLayout = QVBoxLayout()

        # Populate Frame Layout
        pointingHand = qta.icon("fa5.hand-point-up", color="#666666")
        pointingHandWidget = QLabel()
        pointingHandWidget.setPixmap(pointingHand.pixmap(128, 128))
        pointingHandWidget.setAlignment(Qt.AlignBottom | Qt.AlignHCenter)
        callToAction = QLabel("Select an Option to continue.")
        callToAction.setStyleSheet(
            """
                color: #666666;
                font-size: 22pt;
            """
        )
        callToAction.setAlignment(Qt.AlignTop | Qt.AlignHCenter)
        boxLayout.addWidget(pointingHandWidget)
        boxLayout.addWidget(callToAction)

        # Set Frame Layout
        viewFrame.setLayout(boxLayout)

        return viewFrame

    def _getControlFrame(self) -> QFrame:
        # Initialize Control Frame
        controlFrame = QFrame()
        controlFrame.setFrameShape(QFrame.StyledPanel)
        controlFrame.setMaximumHeight(300)
        controlFrameInner = QFrame()
        controlFrameInner.setMaximumWidth(1000)

        # Initialize Frame Layout
        controlFrameLayout = QVBoxLayout()
        controlFrameInnerLayout = QHBoxLayout()

        # Populate Frame Layout
        lowerGraphic = QPixmap("assets\img\png\\read_the_world.png")
        lowerGraphic = lowerGraphic.scaledToWidth(500)
        lowerGraphicWidget = QLabel()
        lowerGraphicWidget.setPixmap(lowerGraphic)
        controlBoxLayout = QVBoxLayout()

        # Aggregation Name Input Box
        self.aggregationNameBox = QLineEdit()
        self.aggregationNameBox.setStyleSheet("""
            font-size: 18pt;
            border-radius: 8px;
            padding: 8px;
            border: 1px solid #DCDCDC;
        """)
        controlBoxLayout.addWidget(self.aggregationNameBox, alignment=Qt.AlignBottom)

        # Start Aggregation Button
        startAggregationButton = QPushButton("New Aggregation")
        startAggregationButton.clicked.connect(self._startAggregationCreation)
        startAggregationButton.setStyleSheet("""
            QPushButton { 
                background-color: #007bff; 
                color: white; 
                font-size: 18pt; 
                border-radius: 8px; 
                padding: 10px;     
            }
        """)
        startAggregationButton.setCursor(Qt.PointingHandCursor)
        controlBoxLayout.addWidget(startAggregationButton, alignment=Qt.AlignTop)

        # Go to My Aggregations Button
        myAggregationsButton = QPushButton("Go to My Aggregations")
        myAggregationsButton.setStyleSheet("""
            QPushButton { 
                background-color: black; 
                color: white; 
                font-size: 18pt;
                border-radius: 8px;
                padding: 10px;
            }
        """)
        myAggregationsButton.setCursor(Qt.PointingHandCursor)
        myAggregationsButton.setFixedHeight(105)
        myAggregationsButton.clicked.connect(self._redirectToArchive)
        controlBoxLayout.addWidget(myAggregationsButton)

        # Initializing the control box widget
        controlBox = QGroupBox()
        controlBox.setObjectName("controlBox")
        controlBox.setStyleSheet("QGroupBox#controlBox { background-color: whitesmoke; border-radius: 8px; border: 1px solid gainsboro; }")
        controlBox.setGraphicsEffect(CommonEffects.createGroupBoxShadow())
        controlBox.setFixedHeight(250)
        controlBox.setMinimumWidth(475)
        controlBox.setLayout(controlBoxLayout)
        controlFrameInnerLayout.addWidget(lowerGraphicWidget)
        controlFrameInnerLayout.addWidget(controlBox)

        # Set Frame Layout
        controlFrameInner.setLayout(controlFrameInnerLayout)
        controlFrameLayout.addWidget(controlFrameInner, alignment=Qt.AlignCenter)
        controlFrame.setLayout(controlFrameLayout)

        return controlFrame

    def _startAggregationCreation(self):
        aggregation_name = self.aggregationNameBox.text()

        # If the input field is empty, abort
        if len(aggregation_name) == 0 : return

        # Create a New Aggregation
        res = requests.post(f"{Config.API_BASE_ENDPOINT}/addAggregation", json={
            "auth_token": self._auth_token,
            "user_id": self._user_id,
            "header": aggregation_name
        })
        deserialized_res = res.json()
        aggregation_id = deserialized_res["aggregation_id"]

        self._set_active_aggregation(aggregation_id, aggregation_name)
        self._switch_to_page(4)

    def _redirectToArchive(self):
        self._switch_to_page(3)

