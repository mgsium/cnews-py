import requests
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QDialog, QVBoxLayout, QLabel, QScrollArea, QFrame, QMainWindow, QToolBar, QPushButton

from config import Config

class ArticlePreviewDialog(QDialog):
    """Dialog for previewing an article before adding to an aggregation."""

    def __init__(self, aggregation_id, article_url, auth_token, refresh_article_table):
        super().__init__()

        self.setWindowTitle(f"Article Preview: {article_url}")
        self.setWindowIcon(QIcon("assets\img\png\cn_icon.png"))
        self.setFixedWidth(640)
        self.setMaximumHeight(700)

        self._aggregation_id = aggregation_id
        self._auth_token = auth_token
        self._refresh_article_table = refresh_article_table
        self._loadArticle(article_url)

        layout = QVBoxLayout()
        layout.setSpacing(0)
        layout = self._setDialogHeader(layout, "Loading...")

        self.setLayout(layout)
        self.show()

    def _setDialogHeader(self, layout: QVBoxLayout, article_header: str) -> QVBoxLayout:
        header = QLabel(f"<h2>{article_header}</h2>")
        header.setStyleSheet("QLabel { font-weight: bold }")
        header.setWordWrap(True)
        header.setAlignment(Qt.AlignCenter)
        layout.addWidget(header)
        return layout

    def _setSuccessNote(self, layout: QVBoxLayout) -> QVBoxLayout:
        successNote = QLabel("Article Successfully Retrieved")
        successNote.setObjectName("successNote")
        successNote.setStyleSheet("QLabel#successNote { color: green }")
        successNote.setAlignment(Qt.AlignCenter)
        layout.addWidget(successNote)
        return layout

    def _setArticleContents(self, layout: QVBoxLayout, article_text: str) -> QVBoxLayout:
        scrollArea = QScrollArea()
        scrollArea.setFixedWidth(620)
        articleTextWidget = QLabel(article_text)
        articleTextWidget.setMaximumWidth(scrollArea.width() - 20)
        articleTextWidget.setStyleSheet("QLabel { padding: 10px; background: white; }")
        articleTextWidget.setWordWrap(True)
        scrollArea.setWidget(articleTextWidget)
        layout.addWidget(scrollArea)
        return layout

    def _insertHorizontalLine(self, layout: QVBoxLayout) -> QVBoxLayout:
        horizontalDividingLine = QFrame()
        horizontalDividingLine.setFrameShape(QFrame.HLine)
        horizontalDividingLine.setFrameShadow(QFrame.Sunken)
        horizontalDividingLine.setContentsMargins(0, 10, 0, 10)
        layout.addWidget(horizontalDividingLine)

        return layout

    def _addControlBtns(self, layout: QVBoxLayout) -> QVBoxLayout:
        # Button to add article to current aggregation
        addBtn = QPushButton("Add to Aggregation")
        addBtn.setObjectName("addBtn")
        addBtn.setStyleSheet("""
            QPushButton#addBtn {
                background-color: #4285F4;
                color: white;
                border: 0px;
                padding: 10px;
                font-size: 14pt;
            }
        """)
        addBtn.mouseReleaseEvent = self._addArticleToActiveAggregation
        addBtn.setCursor(Qt.PointingHandCursor)
        layout.addWidget(addBtn)

        # Button to close the Article Preview
        exitBtn = QPushButton("Exit Preview")
        exitBtn.setObjectName("exitBtn")
        exitBtn.setStyleSheet("""
            QPushButton#exitBtn {
                background-color: #EF233C;
                color: white;
                border: 0px;
                padding: 10px;
                font-size: 14pt;
            }
        """)
        exitBtn.mouseReleaseEvent = lambda e: self.close()
        exitBtn.setCursor(Qt.PointingHandCursor)
        layout.addWidget(exitBtn)

        return layout


    def _loadArticle(self, article_url: str) -> None:
        # Make request to retrieve article.
        res = requests.get(f"{Config.API_BASE_ENDPOINT}/getArticleContents", json = {
            "url": article_url
        })
        deserialized_res = res.json()
        wasSuccessful = (deserialized_res["result"] == "success")
        if not wasSuccessful:
            self._showError()
        else:
            # Assign article to instance variable
            self._article = deserialized_res["data"]["article"]
            # Create and populate Dialog Layout
            layout = QVBoxLayout()
            layout = self._setDialogHeader(layout, self._article["header"])
            layout = self._setSuccessNote(layout)
            layout = self._insertHorizontalLine(layout)
            layout = self._setArticleContents(layout, self._article["text"])
            layout = self._addControlBtns(layout)
            self.setLayout(layout)

    def _addArticleToActiveAggregation(self, event):
        res = requests.post(f"{Config.API_BASE_ENDPOINT}/addArticle", json={
            "aggregation_id": self._aggregation_id,
            "header": self._article["header"],
            "body": self._article["text"],
            "auth_token": self._auth_token
        })
        self._refresh_article_table()
        self.close()