<h1 align="center">
    Central News Python Client
</h1>
<h6 align="center">
    A Level Computer Science NEA - Musab Guma'a
</h6>

Uses the <a href="https://gitlab.com/mgsium/cnews-api">Central News API</a>.

#### To run locally

Execute `python main.py` for `>3.6`
