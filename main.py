# Third Party Libs
from PyQt5.QtWidgets import QApplication
from PyQt5.QtGui import QFontDatabase, QFont

# Standard Libs
import sys

# Local Libs
from components.MainWindow import MainWindow

if __name__ == "__main__":
    # Initializing PyQt5
    app = QApplication([])
    
    # Setting Fonts  
    fontDb = QFontDatabase()
    fontDb.addApplicationFont("Fonts\Spartan\Spartan-VariableFont_wght.ttf")
    fontDb.addApplicationFont("Fonts\Sanchez\Sanchez-Regular.ttf")
    fontDb.addApplicationFont("Fonts\Jost\Jost-VariableFont_wght.ttf")
    app.setFont(QFont("Jost"))

    # Initializing the Main Window
    mainWindow = MainWindow()
    app.exec()
